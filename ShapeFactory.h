#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H

#include <vector>
#include "IShape.h"

enum class ShapeEnum{
   SHAPE_CIRCLE, SHAPE_RECTANGLE, SHAPE_TRIANGLE
};

class ShapeFactory{

	public:
      
      IShape* CreateShapeInstance ( ShapeEnum a_eShape );
      
      //returns factory singleton instance
      static ShapeFactory* Instance();
      
      //deletes all shapes created by factory 
      void Destroy();
      
      bool IsFactoryChildren (IShape* a_oShapeObj);
      
      ~ShapeFactory();
      
   private:
      //to keep track of all produced shapes
      std::vector< IShape* > m_vShapes;
      
      //single pointer
      static ShapeFactory* m_pInstance;
      
      //private constructors to prevent making more than one instances
      ShapeFactory (){};
      ShapeFactory (ShapeFactory const&){};
      //assignment operator is private too
      ShapeFactory& operator= ( ShapeFactory const&){};
};

#endif