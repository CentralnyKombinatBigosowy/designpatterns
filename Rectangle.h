#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "IShape.h"

class Rectangle : public IShape {
   
public:
   Rectangle();

   void setWidth( double a_dWidth);
   void setHeight( double a_dHeight);
   
   double getWidth();
   double getHeight();
   
   double getArea();
   
private:
   double m_dWidth ;
   double m_dHeight ;


};


#endif