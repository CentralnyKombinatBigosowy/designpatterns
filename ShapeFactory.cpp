#include "ShapeFactory.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"

ShapeFactory* ShapeFactory::m_pInstance = nullptr;  

IShape* ShapeFactory::CreateShapeInstance ( ShapeEnum a_eShape ) {
   IShape* result = nullptr;
         
   switch( a_eShape){
      case ShapeEnum::SHAPE_CIRCLE:
         result = new Circle();
         break;
      case ShapeEnum::SHAPE_RECTANGLE:
         result = new Rectangle(); 
         break;
      case ShapeEnum::SHAPE_TRIANGLE:
         result = new Triangle();
         break; 
      default:
         break;
   }
   if(result != nullptr){
      m_vShapes.push_back( result);
   }

   return result;
}

void ShapeFactory::Destroy() {
   //deallocate all pointers 
   for( auto s : m_vShapes) {
      delete s;
   }
   m_vShapes.clear();
}

ShapeFactory* ShapeFactory::Instance(){
   if( m_pInstance == nullptr ){
      //factory requested for the first time 
      m_pInstance = new ShapeFactory();
   }
   return m_pInstance;
}

bool ShapeFactory::IsFactoryChildren (IShape* a_oShapeObj){
   bool found = false; 
   
   for( auto const& s : m_vShapes){
      if( s == a_oShapeObj ) {
         found = true;
         break;
      }
   }
}

ShapeFactory::~ShapeFactory(){
   // call destroy as it already implements cleanup
   Destroy();
}
