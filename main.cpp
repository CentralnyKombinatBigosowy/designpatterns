#include <iostream>
#include <cassert>
#include <cmath>

#include "ShapeFactory.h"
#include "IShape.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"

int main(int argc, char** argv) {
   double area = 0.0;

   std::cout << "Testing circle" << "\n";
   // create circle
   IShape* circ = ShapeFactory::Instance()->CreateShapeInstance(
      ShapeEnum::SHAPE_CIRCLE);
   
   // check if object was created    
   assert( circ != nullptr);
   
   // smaller dimension decides circle diameter 
   circ->setWidth( 10.0);
   circ->setHeight( 4.0);
   
   assert( circ->getWidth() == 10.0);
   assert( circ->getHeight() == 4.0);

   area = circ->getArea();
   std::cout << "Circle area = " << area << " expxdected: " << "12.56637061436" 
      << "\n"; 
   // floating point operation error needs to be considered
   assert( abs(area - 12.56637061436) < 0.0001 );
   
   area = 0.0;
   circ->setWidth( 20.0);
   circ->setHeight( 100.0);
  
   assert( circ->getWidth() == 20.0);
   assert( circ->getHeight() == 100.0);
   
   area = circ->getArea(); 
   std::cout << "Circle area = " << area << " expected: " << "314.15926535898" 
      << "\n"; 
   // floating point operation error needs to be considered
   assert( abs(area - 314.15926535898) < 0.0001);
 
   // --------------------------------------------------------------------------
   std::cout << "Testing rectangle" << "\n";

   // create rectangle 
   IShape* rect = ShapeFactory::Instance()->CreateShapeInstance(
      ShapeEnum::SHAPE_RECTANGLE);
   
   // check if object was created    
   assert( rect != nullptr);
 
   rect->setWidth( 10.0);
   rect->setHeight( 2.0);
   
   assert( rect->getWidth() == 10.0);
   assert( rect->getHeight() == 2.0);
   
   area = 0.0;
   area = rect->getArea();

   std::cout << "Rectangle area = " << area << " expected: " << "20.0" << "\n"; 
   assert( area == 20.0);
   
   rect->setWidth( 100.0);
   rect->setHeight( 0.5);
   area = 0.0;
   area = rect->getArea();   
   std::cout << "Rectangle area = " << area << " expected: " << "50.0" << "\n";     
   assert( area == 50.0);

   // --------------------------------------------------------------------------
   std::cout << "Testing triangle" << "\n";
   
   // create triangle    
   IShape* tria = ShapeFactory::Instance()->CreateShapeInstance(
      ShapeEnum::SHAPE_TRIANGLE);

   // check if object was created    
   assert( tria != nullptr);

   tria->setWidth( 30.0);
   tria->setHeight( 30.0);
   
   area = 0.0;
   area = tria->getArea();
   std::cout << "Rectangle area = " << area << " expected: " << "450.0" << "\n"; 
   assert( area == 450.0);

   tria->setWidth( 3.0);
   tria->setHeight( 1.0);
   
   assert( tria->getWidth() == 3.0);
   assert( tria->getHeight() == 1.0);
   
   area = 0.0;
   area = tria->getArea();  
   
   std::cout << "Rectangle area = " << area << " expected: " << "1.5" << "\n";    
   assert( area == 1.5);
   
   // --------------------------------------------------------------------------
   // check if factory returns null ptr if shape is not known
   IShape* unknowShape = ShapeFactory::Instance()->CreateShapeInstance(
      (ShapeEnum) 10); 
   assert( unknowShape == nullptr);
   std::cout << "Factory returns nullptr for unknown shape \n";    


   // --------------------------------------------------------------------------
   // test if factory remembers it's products
   std::cout << "Testing IsFactoryChildren positive cases" << "\n";
   bool isChildren = false;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( circ);
   assert( isChildren == true);
   
   isChildren = false;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( rect);
   assert( isChildren == true);
   
   isChildren = false;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( tria);
   assert( isChildren == true);
   
   std::cout << "Positive cases ok" << "\n";
   std::cout << "Testing IsFactoryChildren negative cases" << "\n";

   IShape* orphan = new Circle();
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( orphan);
   delete orphan;
   assert( isChildren == false);

   orphan = new Rectangle();
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( orphan);
   delete orphan;
   assert( isChildren == false);
  
   orphan = new Triangle();
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( orphan);
   delete orphan;
   assert( isChildren == false);
  
   Circle* orphanCircle = new Circle();
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( orphanCircle);
   delete orphanCircle;
   assert( isChildren == false);
   
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( nullptr);
   assert( isChildren == false);
   
   std::cout << "Negative cases ok" << "\n";
   
   // --------------------------------------------------------------------------
   // check destroy. I'm not sure how to show that memory was freed  
   // without external software 
   std::cout << "Check destroy" << "\n";

   ShapeFactory::Instance()->Destroy();
   
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( rect);
   assert( isChildren == false);
   std::cout << "rect no longer points at any of factory children" << "\n";

   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( tria);
   assert( isChildren == false);
   std::cout << "tria no longer points at any of factory children" << "\n";
   
   isChildren = true;
   isChildren = ShapeFactory::Instance()->IsFactoryChildren( circ);
   assert( isChildren == false);
   std::cout << "circ no longer points at any of factory children" << "\n";
   
   std::cout << "destroy ok" << "\n";
   
   // to check if program is leaking memory 
   for( int i = 0; i < 10000; ++i){
      IShape* shape = ShapeFactory::Instance()->CreateShapeInstance(
         ShapeEnum::SHAPE_RECTANGLE);
   }
      

   // cleanup
   ShapeFactory::Instance()->Destroy();
   return 0;
} 
