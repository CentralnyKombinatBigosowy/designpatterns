#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "IShape.h"

class Triangle : public IShape {
   
public:
   Triangle();

   void setWidth( double a_dWidth);
   void setHeight( double a_dHeight);
   
   double getWidth();
   double getHeight();
   
   double getArea();
   
private:
   double m_dWidth ;
   double m_dHeight ;


};


#endif