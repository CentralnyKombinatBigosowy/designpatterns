#ifndef CIRCLE_H
#define CIRCLE_H

#include "IShape.h"

class Circle : public IShape {
   
public:
   Circle();

   void setWidth( double a_dWidth);
   void setHeight( double a_dHeight);
   
   double getWidth();
   double getHeight();
   
   double getArea();
   
private:
   double m_dWidth ;
   double m_dHeight ;


};


#endif