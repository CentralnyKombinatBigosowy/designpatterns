#include "Rectangle.h"

Rectangle::Rectangle(){
   m_dHeight = 0.0;
   m_dWidth  = 0.0;
}

void Rectangle::setWidth( double a_dWidth){ 
	m_dWidth = a_dWidth;
}

void Rectangle::setHeight( double a_dHeight){
	m_dHeight = a_dHeight;
}
   
double Rectangle::getWidth(){
	return m_dWidth;
}

double Rectangle::getHeight(){
	return m_dHeight;
}


double Rectangle::getArea(){
	return m_dHeight * m_dWidth;
}
