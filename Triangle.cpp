#include "Triangle.h"

Triangle::Triangle(){
   m_dHeight = 0.0;
   m_dWidth  = 0.0;
}

void Triangle::setWidth( double a_dWidth){ 
	m_dWidth = a_dWidth;
}

void Triangle::setHeight( double a_dHeight){
	m_dHeight = a_dHeight;
}
   
double Triangle::getWidth(){
	return m_dWidth;
}

double Triangle::getHeight(){
	return m_dHeight;
}


double Triangle::getArea(){
	return m_dHeight * m_dWidth * 0.5;
}
