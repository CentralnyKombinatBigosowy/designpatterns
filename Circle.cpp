#include "Circle.h"

//trying to use standard pi, if it doesn't work declare define it
#define _USE_MATH_DEFINES
#include <cmath>
#ifndef M_PI
 #define M_PI 3.14159265358979323846264338327
#endif 

Circle::Circle(){
   m_dHeight = 0.0;
   m_dWidth  = 0.0;
}

void Circle::setWidth( double a_dWidth){ 
	m_dWidth = a_dWidth;
}

void Circle::setHeight( double a_dHeight){
	m_dHeight = a_dHeight;
}
   
double Circle::getWidth(){
	return m_dWidth;
}

double Circle::getHeight(){
	return m_dHeight;
}

// The Circle diameter is assumed to be equal to shape smaller dimension, 
// so it always occupies the maximum avalible area inside bounding box
double Circle::getArea(){
   // I should probably check for overflows and negative dimensions here, but 
   // don't want to mess with POSIX functions or std exceptions 
	double radius;
	if( m_dHeight > m_dWidth ) radius = m_dWidth / 2.0;
	else radius = m_dHeight  / 2.0;
	return (M_PI * radius * radius);
}
